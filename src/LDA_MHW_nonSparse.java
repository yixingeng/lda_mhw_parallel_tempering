import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/** This is a non-sparse version of Metropolis-Hastings-Walker LDA for development and debugging purposes.
 *  It does not have any of the speed advantages of the full algorithm.
 * @author James Foulds
 *
 */
public class LDA_MHW_nonSparse {
	
	protected final int numTopics;
	protected final int numWords;
    protected int numDocuments;
	
	protected final double beta;
	protected final double betaSum;
    protected double[] alpha;
    
    protected int[][] wordTopicCounts; //[numWords][numTopics]
    protected int[] topicCounts; //numTopics;
    protected int[][] docTopicCounts; //[numDocuments][numTopics]
    
    protected int[][] topicAssignments; //[document][word]
    
    protected Random random = new Random();


	final int MHWnumCachedSamples;
	int[][] MHWcachedSamples; //[word][sample]
	int[] MHWwordSampleInds; //[word]
	
	//store these in the class to avoid re-allocating them, but their values will not be read
	//outside of the creation of the samples via the Walker algorithm
	private double[] MHWtempDistribution;
	private double[][] MHWaliasTable;
	private double[][] MHW_L;
	private double[][] MHW_H;
	
	double[][] MHWstaleDistributions; //[word][K]
	double[] MHWnormalizationConstants;//[word] Corresponds to Q_w in the Metropolis Hastings Walker paper
	
	private boolean doAnnealing;
	private double annealingAlpha = 0.99;
	private double annealingFinalTemperature = 1;
	private double annealingLambda = 10;
	private int iterationNumber = 0;

	public LDA_MHW_nonSparse(int numTopics, int numWords, double beta, double[] alpha, boolean doAnnealing) {
    	this.numTopics = numTopics;
    	this.numWords = numWords;
    	this.beta = beta;
    	this.betaSum = beta * numWords;
    	this.alpha = alpha;
    	assert(alpha.length == numTopics);
		MHWnumCachedSamples = numTopics;//1000; //FIXME!!!//numTopics;
		MHWtempDistribution = new double[numTopics];
		MHWaliasTable = new double[numTopics][2];
		MHW_L = new double[numTopics][2];
		MHW_H = new double[numTopics][2];
		MHWcachedSamples = new int[numWords][MHWnumCachedSamples];
		this.setDoAnnealing(doAnnealing); 
    }
	
    public void doMCMC(int[][] documents, int numIterations, boolean saveLastSample) {
    	numDocuments = documents.length;
    	initialize(documents);
    	
    	for (int i = 0; i < numIterations; i++) {
    		System.out.print("Iteration " + (i + 1));
    		long startTime = System.currentTimeMillis();
    		updateTopicAssignments(documents);
    		long finishTime = System.currentTimeMillis();
    		System.out.println(", " + (finishTime - startTime)/1000.0 + " seconds");
    	}
    	
    	if (saveLastSample) {
    		try {
				saveToText("LDATopicModel_");
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    }
    
    public void doMCMC(File file, int numDocuments, int numIterations, boolean saveLastSample) throws IOException {
    	//Load from file
    	int[][] documents = new int[numDocuments][];
    	BufferedReader inputStream = null;
    	
    	int j = 0;
    	try {
            inputStream = new BufferedReader(new FileReader(file));

            String l;
            while ((l = inputStream.readLine()) != null) {
            	if (l.length() == 0) {
            		documents[j] = new int[0];
            		j = j + 1;
            		continue;
            	}
                String[] split = l.split(" ");
                documents[j] = new int[split.length];
                for (int k = 0; k < split.length; k++) {
                	documents[j][k] = Integer.parseInt(split[k]);
                }
                j = j + 1;
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    	
    	doMCMC(documents, numIterations, saveLastSample);
    }
        
    
    /** Compute the sufficient statistics from scratch. */
    protected void recomputeTextSufficientStatistics(int[][] documents) {
    	
        wordTopicCounts = new int[numWords][numTopics];
        topicCounts = new int[numTopics];
        docTopicCounts = new int[numDocuments][numTopics];

        int word;
        int topic;
        for (int i = 0; i < numDocuments; i++) {
            for (int j = 0; j < documents[i].length; j++) {           
               word = documents[i][j];
               topic = topicAssignments[i][j];
               
               wordTopicCounts[word][topic]++;
               topicCounts[topic]++;
               docTopicCounts[i][topic]++;
            }
        }
    }
    
    public int[][] getWordTopicCounts() {
    	return wordTopicCounts;
    }

    public int[] getTopicCounts() {
    	return topicCounts;
    }
    
    public int[][] getDocTopicCounts() {
    	return docTopicCounts;
    }
    
    public int[][] getTopicAssignments() {
    	return topicAssignments;
    }
    
    public void saveToText(String baseFilename)  throws IOException {
    	final String topicAssignmentName = "topicAssignments.txt";
    	final String wordTopicCountsName = "wordTopicCounts.txt";
    	final String docTopicCountsName = "docTopicCounts.txt";
    	
    	PrintWriter outputStream = null;
    	try {
            outputStream = new PrintWriter(baseFilename + topicAssignmentName);
        	for (int i = 0; i < topicAssignments.length; i++) {
     	       //sample the topic assignments for each word in the ith document
     	       for (int n = 0; n < topicAssignments[i].length; n++) {
     	    	  outputStream.print(topicAssignments[i][n]);
     	    	  if (n < topicAssignments[i].length) {
     	    		 outputStream.print(" ");
     	    	  }
     	       }
     	      outputStream.println("");
        	}   
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    	
    	int[][] wordTopicCounts = getWordTopicCounts();
    	
    	outputStream = null;
    	try {
            outputStream = new PrintWriter(baseFilename + wordTopicCountsName);
        	for (int i = 0; i < wordTopicCounts.length; i++) {
     	       for (int n = 0; n < wordTopicCounts[i].length; n++) {
     	    	  outputStream.print(wordTopicCounts[i][n]);
     	    	  if (n < wordTopicCounts[i].length) {
     	    		 outputStream.print(" ");
     	    	  }
     	       }
     	      outputStream.println("");
        	}
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    	
    	int[][] docTopicCounts = getDocTopicCounts();
    	
    	outputStream = null;
    	try {
            outputStream = new PrintWriter(baseFilename + docTopicCountsName);
        	for (int i = 0; i < docTopicCounts.length; i++) {
     	       for (int n = 0; n < docTopicCounts[i].length; n++) {
     	    	  outputStream.print(docTopicCounts[i][n]);
     	    	  if (n < docTopicCounts[i].length) {
     	    		 outputStream.print(" ");
     	    	  }
     	       }
     	      outputStream.println("");
        	}
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

	
	protected int getNextSample(int word) {
		int returner;
		int sampleInd = MHWwordSampleInds[word];
		if (sampleInd < MHWnumCachedSamples) {
			returner = MHWcachedSamples[word][sampleInd];
			MHWwordSampleInds[word]++;
			return returner;
		}
		else {
			//Draw new samples via Walker's alias method

			//construct new distribution
			double normalizer = 0;
			for (int k = 0; k < numTopics; k++) {
				MHWtempDistribution[k]  = alpha[k] * (wordTopicCounts[word][k] + beta)/(topicCounts[k] + betaSum);
				normalizer += MHWtempDistribution[k];
			}
			for (int k = 0; k < numTopics; k++) {
				MHWtempDistribution[k] /= normalizer;
				
				//save the (soon to be) stale distribution.
				MHWstaleDistributions[word][k] = MHWtempDistribution[k];
			}
			MHWnormalizationConstants[word] = normalizer; //We need this for the proposal
			
			ProbabilityUtils.generateAlias(MHWtempDistribution, MHWaliasTable, MHW_L, MHW_H); //create alias table
			ProbabilityUtils.sampleAlias(MHWaliasTable, MHWcachedSamples[word]); //get samples

			returner = MHWcachedSamples[word][0]; //the first new sample
			MHWwordSampleInds[word] = 1; //the one after it
			return returner;
		}
	}
	
	protected void initialize(int[][] documents) {
    	topicAssignments = new int[numDocuments][];
        for (int i = 0; i < numDocuments; i++) {
        	topicAssignments[i] = new int[documents[i].length];
        	//initialize topic assignments uniformly at random
        	for (int j = 0; j < topicAssignments[i].length; j++) {
        		topicAssignments[i][j] = random.nextInt(numTopics);
        	}
        }
        
        //initialize count matrices
        recomputeTextSufficientStatistics(documents);

        MHWstaleDistributions = new double[numWords][numTopics];
        MHWnormalizationConstants = new double[numWords];
        
        MHWwordSampleInds = new int[numWords];
        for (int i = 0; i < numWords; i++) {
        	MHWwordSampleInds[i] = MHWnumCachedSamples;
        	getNextSample(i); //creates samples for this word and sets the mixture weight for it
        	MHWwordSampleInds[i] = 0;
        }
    }
	
	protected void updateTopicAssignments(int[][] documents) {
    	//for locality of reference, and to avoid reallocation
    	int oldTopic;
    	int newTopic;
    	int word;
    	    	
    	double pi; //Metropolis-Hastings acceptance probability
    	int accepts = 0;
    	int rejects = 0;
    	double sparseNormalizer;
    	double[] sparseDistribution = new double[numTopics];
    	boolean chooseSparse;
    	
    	double temperature = 1; //for simulated annealing
    	iterationNumber++;
    	
    	long time1 = 0; 
    	long time2 = 0;
    	long time3 = 0;
    	long time4 = 0;
    	long time5 = 0;
    	long startTime;
 	    accepts = 0;
 	    rejects = 0;
    	
    	for (int i = 0; i < documents.length; i++) {
	       //sample the topic assignments for each word in the ith document
	       for (int n = 0; n < documents[i].length; n++) {
	           //remove the current assignment from the sufficient statistics cache
	    	   startTime = System.nanoTime();
	    	   
	           oldTopic = topicAssignments[i][n];
	           word = documents[i][n];
	           
	           wordTopicCounts[word][oldTopic]--;
	           topicCounts[oldTopic]--;
	           docTopicCounts[i][oldTopic]--;
	           	           
	           time1 += System.nanoTime() - startTime;
	           startTime = System.nanoTime();
	           
	           //choose sparse and non-sparse paths from proposal mixture (naive, non-sparse, slow!!)
	           sparseNormalizer = 0;
	           
	           for (int k = 0; k < numTopics; k++) {
	        	   sparseDistribution[k] = docTopicCounts[i][k] * (wordTopicCounts[word][k] + beta)/(topicCounts[k] + betaSum);
	        	   sparseNormalizer += sparseDistribution[k];
	           }
	           chooseSparse = random.nextDouble() < sparseNormalizer / (sparseNormalizer + MHWnormalizationConstants[word]);
	           
	           time2 += System.nanoTime() - startTime;
	           startTime = System.nanoTime();

	           //Draw from proposal
	           if (chooseSparse) {
	        	   //naive, non-sparse, slow!!
	        	   newTopic = ProbabilityUtils.sampleFromDiscrete(sparseDistribution, sparseNormalizer);
	           } else {
	        	   newTopic = getNextSample(word); //Draw from alias table
	           }
	           
	           time3 += System.nanoTime() - startTime;
	           startTime = System.nanoTime();
	        	   
	           //Compute Metropolis-Hastings acceptance ratio
	           //model portion of proposal
	           pi = Math.log(docTopicCounts[i][newTopic] + alpha[newTopic]) - Math.log(wordTopicCounts[i][oldTopic] + alpha[oldTopic])
	        		   + Math.log(wordTopicCounts[word][newTopic] + beta) - Math.log(wordTopicCounts[word][oldTopic] + beta);
	           	
	           //The temperature only affects the model portion of the acceptance ratio
	           if (getDoAnnealing()) {
	        	   //simulated annealing, change temperature
		           temperature = getAnnealingFinalTemperature() + getAnnealingLambda() * Math.pow(getAnnealingAlpha(), iterationNumber);
		           pi /= temperature;
	           }
	           
	           //contribution from proposal
	           pi += Math.log(sparseNormalizer * sparseDistribution[oldTopic] + MHWnormalizationConstants[word] * MHWstaleDistributions[word][oldTopic]) - Math.log(sparseNormalizer * sparseDistribution[newTopic] + MHWnormalizationConstants[word] * MHWstaleDistributions[word][newTopic]);

	           //Accept or reject proposal
	           pi = Math.min(1, Math.exp(pi));
			   if (random.nextDouble() < pi) {
				   topicAssignments[i][n] = newTopic;
				   accepts = accepts + 1;
			   }
			   else {
				   newTopic = oldTopic;
				   rejects = rejects + 1;
			   }
	           
	           time4 += System.nanoTime() - startTime;
	           startTime = System.nanoTime();

	           //update sufficient statistics cache
	           topicCounts[newTopic]++;
	           wordTopicCounts[word][newTopic]++;
	           docTopicCounts[i][newTopic]++;
	           //checkTopicCountsForWords(documents); //TESTING!!!
	           time5 += System.nanoTime() - startTime;
	       }
    	}
    	System.out.print(", accept rate: " + accepts /(accepts + rejects + 0.0));
    	System.out.print(", temperature: " + temperature);
    	System.out.print(", time: " + TimeUnit.NANOSECONDS.toMillis(time1) + ", " + TimeUnit.NANOSECONDS.toMillis(time2) + ", " + TimeUnit.NANOSECONDS.toMillis(time3) + ", " + TimeUnit.NANOSECONDS.toMillis(time4) + ", " + TimeUnit.NANOSECONDS.toMillis(time5) + " ms per section");
    	System.out.print(", total time: " + TimeUnit.NANOSECONDS.toMillis(time1 + time2 + time3 + time4 + time5) + "ms");
    }
		
    
    public static void main (String[] args) {
   	
    	/*String workingDir = "D:\\jimmy\\research\\UCSD\\mixedMembershipWordEmbeddings\\results_test\\results_SOTU\\";
    	String datasetFilename = "SOTUforJavafull.txt";
    	String MMSGTMbaseFilename = "MMskipGramTopicModel_";
    	
    	int numWords = 7384;
        int numTopics = 500;
        int numDocuments = 225;
    	*/
    	
        String workingDir = "D:\\jimmy\\research\\UCSD\\annealedMHW\\results_test\\sandbox\\NIPS\\";
    	String datasetFilename = "NIPSforJavafull.txt";
    	String MMSGTMbaseFilename = "LDA_";
        
        int numWords = 13649;
        int numTopics = 200;//2000;
        int numDocuments = 1740;
        
        /*String workingDir = "D:\\jimmy\\research\\UCSD\\mixedMembershipWordEmbeddings\\results_test\\results_Federalist\\";
        String datasetFilename = "FederalistforJavafull.txt";
        String MMSGTMbaseFilename = "MMskipGramTopicModel_";
    	int numWords = 4818;
        int numTopics = 100;
        int numDocuments = 85;*/
    	
    	/*String workingDir = "D:\\jimmy\\research\\UCSD\\mixedMembershipWordEmbeddings\\results_test\\results_Shakespeare\\";
        String datasetFilename = "ShakespeareforJavafull.txt";
        String MMSGTMbaseFilename = "MMskipGramTopicModel_";
    	int numWords = 5588;
        int numTopics = 100;//500;
        int numDocuments = 1;*/
    	
    	/*String workingDir = "D:\\jimmy\\research\\UCSD\\mixedMembershipWordEmbeddings\\results_test\\results_DuBois\\";
        String datasetFilename = "DuBoisforJavafull.txt";
        String MMSGTMbaseFilename = "MMskipGramTopicModel_";
    	int numWords = 4503;
        int numTopics = 100;
        int numDocuments = 1;*/
        
        
        int numIterations = 1000;//100;
        boolean doAnnealing = true;//false;
        double annealingFinalTemperature = 0.0001;
        double annealingLambda = 3;
        
        double alpha_k = 0.01;
        double beta_w = 0.001;
        
        double[] alpha = new double[numTopics];
        for (int i = 0; i < alpha.length; i++) {
        	alpha[i] = alpha_k;
        }
        
        
        //File file=new File("govTweetsForJava.txt");
        //File file=new File("NIPSfromTextForJava.txt");
    	File file=new File(workingDir + datasetFilename);
        
    	LDA_MHW_nonSparse lda = new LDA_MHW_nonSparse(numTopics, numWords, beta_w, alpha, doAnnealing);
        lda.setAnnealingFinalTemperature(annealingFinalTemperature);
		lda.setAnnealingLambda(annealingLambda);
		
    	try {
			lda.doMCMC(file, numDocuments, numIterations, true);
			lda.saveToText(workingDir + MMSGTMbaseFilename);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	
    }

	double getAnnealingFinalTemperature() {
		return annealingFinalTemperature;
	}

	void setAnnealingFinalTemperature(double annealingFinalTemperature) {
		this.annealingFinalTemperature = annealingFinalTemperature;
	}

	double getAnnealingAlpha() {
		return annealingAlpha;
	}

	void setAnnealingAlpha(double annealingAlpha) {
		this.annealingAlpha = annealingAlpha;
	}

	boolean getDoAnnealing() {
		return doAnnealing;
	}

	void setDoAnnealing(boolean doAnnealing) {
		this.doAnnealing = doAnnealing;
	}

	private double getAnnealingLambda() {
		return annealingLambda;
	}

	private void setAnnealingLambda(double annealingLambda) {
		this.annealingLambda = annealingLambda;
	}

}
