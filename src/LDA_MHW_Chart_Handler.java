
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import java.util.ArrayList;

public class LDA_MHW_Chart_Handler extends ApplicationFrame {
    public LDA_MHW_Chart_Handler(final String title, final ArrayList<ArrayList<EnergyTemp>> totalList) {

        super(title);
        final JFreeChart chart = createChart(totalList);
        final ChartPanel panel = new ChartPanel(chart, true, true, true, false, true);
        panel.setPreferredSize(new java.awt.Dimension(1500, 1270));
        setContentPane(panel);

    }

    private JFreeChart createChart(ArrayList<ArrayList<EnergyTemp>> totalList) {
        // create energy subplot
        final XYDataset data1 = createEnergyDataset(totalList);
        final XYItemRenderer renderer1 = new StandardXYItemRenderer();
        final NumberAxis rangeAxis1 = new NumberAxis("Energy(10^7)");
        rangeAxis1.setAutoRange(true);
        rangeAxis1.setAutoRangeIncludesZero(false);
        final XYPlot subplot1 = new XYPlot(data1, null, rangeAxis1, renderer1);
        subplot1.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);

        // create subplot 2...
        final XYDataset data2 = createTempDataset(totalList);
        final XYItemRenderer renderer2 = new StandardXYItemRenderer();
        final NumberAxis rangeAxis2 = new NumberAxis("Temperature");
        rangeAxis2.setAutoRange(true);
        rangeAxis2.setAutoRangeIncludesZero(false);
        final XYPlot subplot2 = new XYPlot(data2, null, rangeAxis2, renderer2);
        subplot2.setRangeAxisLocation(AxisLocation.TOP_OR_LEFT);

        // parent plot...
        NumberAxis numberAxis = new NumberAxis("Iterations");
        numberAxis.setAutoRangeIncludesZero(false);
        final CombinedDomainXYPlot plot = new CombinedDomainXYPlot(numberAxis);
        plot.setGap(5.0);

        // add the subplots...
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);

        // return a new chart containing the overlaid plot...
        return new JFreeChart("LDA MHW Energy&Temperature",
                JFreeChart.DEFAULT_TITLE_FONT, plot, true);
    }

    private XYDataset createEnergyDataset(ArrayList<ArrayList<EnergyTemp>> totalList) {
        final XYSeriesCollection energyCollection = new XYSeriesCollection();
        for (int i = 0; i < totalList.size(); ++i) {
            final XYSeries series = new XYSeries("E" + i);
            for (int j = 0; j < totalList.get(i).size(); ++j) {
                EnergyTemp et = totalList.get(i).get(j);
                series.add(et.iter, et.energy / (10000000.0));
            }
            energyCollection.addSeries(series);
        }

        return energyCollection;
    }

    private XYDataset createTempDataset(ArrayList<ArrayList<EnergyTemp>> totalList) {
        final XYSeriesCollection tempCollection = new XYSeriesCollection();
        for (int i = 0; i < totalList.size(); ++i) {
            final XYSeries series = new XYSeries("T" + i);
            for (int j = 0; j < totalList.get(i).size(); ++j) {
                EnergyTemp et = totalList.get(i).get(j);
                series.add(et.iter, et.temperature);
            }
            tempCollection.addSeries(series);
        }

        return tempCollection;
    }

}
