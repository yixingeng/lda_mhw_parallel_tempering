import java.io.*;
import java.util.*;


public class LDA_MHW_Manager extends Thread {
    private int numWorkers;
    private int numSwapping;
    private boolean doSwapping;
    protected final int numTopics;
    protected final int numWords;
    protected int numDocuments;

    protected int[][] wordTopicCounts; //[numWords][numTopics]
    protected int[] topicCounts; //numTopics;
    protected int[][] docTopicCounts; //[numDocuments][numTopics]
    protected int[][] topicAssignments; //[document][word]
    private double swappingAcceptanceRate = 0.0;

    protected final double beta;
    protected final double betaSum;
    protected double[] alpha;

    private static int[][] documents;
    private int numIterations;
    private int stepIteration;
    private Random random = new Random();
    private LDA_MHW_Logger logger = new LDA_MHW_Logger("MANAGER");
    private String[] dict;
    private ArrayList<ArrayList<EnergyTemp>> totalList = new ArrayList<>();
    private LDA_MHW_Worker[] workers;
    private HashMap<Double, Integer> tempWorkerMap = new HashMap<>();
    private double[] temperatures;
    private Random rand = new Random();
    private ArrayList<EnergyTemp> bestETlist = new ArrayList<>();
    private int[] bestWorkerId;
    private File topicAssignmentFile;

    private void loadDict(File file) throws IOException {
        BufferedReader inputStream = null;
        dict = new String[numWords];
        try {
            inputStream = new BufferedReader(new FileReader(file));
            String word;
            int i = 0;
            while ((word = inputStream.readLine()) != null) {
                dict[i] = word;
                i++;
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        logger.log("Complete loading dictionary.");
    }

    private void loadDocument(File file, int numDocuments) throws IOException {
        BufferedReader inputStream = null;
        int j = 0;
        documents = new int[numDocuments][];
        try {
            inputStream = new BufferedReader(new FileReader(file));

            String l;
            while ((l = inputStream.readLine()) != null) {
                if (l.length() == 0) {
                    this.documents[j] = new int[0];
                    j = j + 1;
                    continue;
                }
                String[] split = l.split(" ");
                this.documents[j] = new int[split.length];
                for (int k = 0; k < split.length; k++) {
                    this.documents[j][k] = Integer.parseInt(split[k]);
                }
                j = j + 1;
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        logger.log("Complete Document Loading");
    }

    private void exportTopicAssignment(File file) {
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(file);
            for (int i = 0; i < topicAssignments.length; ++i) {
                for (int j = 0; j < topicAssignments[i].length; ++j) {
                    outputStream.print(topicAssignments[i][j]);
                    if (j < topicAssignments[i].length) {
                        outputStream.print(" ");
                    }
                }
                outputStream.println("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
        logger.log("Complete exporting topic assignment.");
    }

    //calculate Metropolis-Hasting Criterion
    private boolean MHCriterion(double t1, double t2, double e1, double e2) {
        double acceptanceRate = Math.min(1, Math.exp((e1 - e2) * (1 / t1 - 1 / t2)));
        double tmpE = e1 - e2;
        double tmpT = (1 / t1) - (1 / t2);
        double res = tmpE * tmpT;
        double finalRes = Math.exp(res);
        logger.log("tmpE:" + tmpE + " tmpT:" + tmpT + " res:" + res + " finalRes:" + finalRes);
        double tmp = random.nextDouble();
        boolean result = (tmp < acceptanceRate);
        logger.log("acceptance rate:" + acceptanceRate + " random number:" + tmp + " swapping: " + result);
        return result;
    }

    public LDA_MHW_Manager(int numTopics, int numWords, double beta, double[] alpha,
                           File docFile, int numDocuments, int numIterations, int numWorkers,
                           int stepIteration, int numSwapping,
                           File dictFile, File topicAssignmentFile, boolean doSwapping) {
        this.numTopics = numTopics;
        this.numWords = numWords;
        this.beta = beta;
        this.betaSum = beta * numWords;
        this.alpha = alpha;
        this.numDocuments = numDocuments;
        this.topicAssignmentFile = topicAssignmentFile;
        this.doSwapping = doSwapping;

        try {
            loadDocument(docFile, this.numDocuments);
            loadDict(dictFile);
            if (!topicAssignmentFile.exists()) {
                /*
                    make an initial topic assignment
                    initialize topic assignments uniformly at random
                    save it as a file
                 */
                this.topicAssignments = new int[numDocuments][];
                for (int i = 0; i < numDocuments; i++) {
                    this.topicAssignments[i] = new int[documents[i].length];
                    for (int j = 0; j < this.topicAssignments[i].length; j++) {
                        this.topicAssignments[i][j] = random.nextInt(numTopics);
                    }
                }
                exportTopicAssignment(topicAssignmentFile);
                topicAssignments = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.numIterations = numIterations;
        this.numWorkers = numWorkers;
        this.stepIteration = stepIteration;
        this.numSwapping = numSwapping;
        this.temperatures = new double[numWorkers];
        this.bestWorkerId = new int[this.numSwapping];

    }

    private void swapTemperatures() {

        //pick two workers and swap temperatures
        int tempIdx1 = rand.nextInt(numWorkers - 1);
        int tempIdx2 = tempIdx1 + 1;
        int wId1 = tempWorkerMap.get(temperatures[tempIdx1]);
        int wId2 = tempWorkerMap.get(temperatures[tempIdx2]);

        double e1 = workers[wId1].getCurEnergy();
        double e2 = workers[wId2].getCurEnergy();
        double t1 = workers[wId1].getTemperature();
        double t2 = workers[wId2].getTemperature();
        logger.log("worker " + wId1 + " temperature: " + t1 + " " + "energy: " + e1 + "\n"
                + "worker " + wId2 + " temperature: " + t2 + " " + "energy: " + e2);

        if (MHCriterion(t1, t2, e1, e2)) {
            workers[wId1].setTemperature(t2);
            workers[wId2].setTemperature(t1);
            tempWorkerMap.put(t2, wId1);
            tempWorkerMap.put(t1, wId2);
            swappingAcceptanceRate += 1.0;
        }
    }

    private void setTemperatures() {
        for (int i = 0; i < numWorkers; ++i) {
            temperatures[i] = 0.5 + i * 1.0 / numWorkers;
            tempWorkerMap.put(temperatures[i], i);
        }
    }

    private void recordBestWorker(int curSwappingNum) {
        logger.log("record best worker");
        double lowestEnergy = Double.MAX_VALUE;
        int workerId = -1;
        double temperature = 0.0;
        for (int i = 0; i < workers.length; ++i) {
            double curEnergy = workers[i].getCurEnergy();
            double curTemp = workers[i].getTemperature();
            if (curEnergy < lowestEnergy) {
                workerId = i;
                lowestEnergy = curEnergy;
                temperature = curTemp;
            }
        }
        bestWorkerId[curSwappingNum] = workerId;
        bestETlist.add(new EnergyTemp((curSwappingNum + 1) * stepIteration, lowestEnergy, temperature));
    }

    public void run() {
        workers = new LDA_MHW_Worker[numWorkers];
        setTemperatures();
        for (int i = 0; i < numWorkers; ++i) {
            int workerId = i;

            workers[i] = new LDA_MHW_Worker(numIterations, temperatures[i], workerId, stepIteration,
                    wordTopicCounts, topicCounts, docTopicCounts, documents, numDocuments, numTopics,
                    numWords, alpha, beta, betaSum, dict, topicAssignmentFile);
            workers[i].start();
        }

        for (int i = 0; i < numSwapping; i++) {
            //wait until all workers stop
            while (true) {
                int allStopFlag = 0;
                for (LDA_MHW_Worker w : workers) {
                    allStopFlag += w.checkStatus();
                }
                if (allStopFlag == 0) break;
            }
            if (doSwapping) {
                swapTemperatures();
            }

            recordBestWorker(i);

            //continue all workers
            for (LDA_MHW_Worker w : workers) {
                w.continueWorker();
            }
        }

        logger.log("Swapping Acceptance Rate: " + swappingAcceptanceRate / numSwapping);

        while (true) {
            boolean flag = false;
            for (int i = 0; i < numWorkers; ++i) {
                if (workers[i].isAlive()) flag = true;
            }
            if (!flag) break;
        }

        for (int i = 0; i < numWorkers; ++i) {
            totalList.add(workers[i].getETlist());
        }

        LDA_MHW_CSV_Handler bestEnergyCSV = new LDA_MHW_CSV_Handler("BestPerformance.csv", true);
        bestEnergyCSV.addEntry(bestETlist, bestWorkerId);
        bestEnergyCSV.writeCSV();

        LDA_MHW_Chart_Handler dataPlot = new LDA_MHW_Chart_Handler("LDA MHW Energy&Temperature", totalList);
        dataPlot.pack();
        dataPlot.setVisible(true);
    }
}

class EnergyTemp {
    public int iter;
    public double energy;
    public double temperature;

    public EnergyTemp(int iter, double energy, double temperature) {
        this.iter = iter;
        this.energy = energy;
        this.temperature = temperature;
    }
}