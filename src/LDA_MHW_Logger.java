import java.util.logging.*;

public class LDA_MHW_Logger {
    public void log(String msg) {
        logger.log(Level.INFO, msg);
    }

    private Logger logger;

    public LDA_MHW_Logger(String logName) {
        logger = Logger.getLogger(logName);

        try {
            FileHandler fileHandler = new FileHandler("./" + logName + ".log");
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
