import com.opencsv.*;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;


public class LDA_MHW_CSV_Handler {
    private String csvName;
    private List<String[]> entries = new ArrayList<>();

    public LDA_MHW_CSV_Handler(String csvName) {
        String[] titles = {"iteration", "energy", "temperature"};
        this.csvName = csvName;
        entries.add(titles);
    }

    public LDA_MHW_CSV_Handler(String csvName, boolean flag) {
        String[] titles = {"id", "iteration", "energy", "temperature"};
        this.csvName = csvName;
        entries.add(titles);
    }

    public void addEntry(ArrayList<EnergyTemp> ETList, int[] bestWorkerId) {
        for (int i = 0; i < bestWorkerId.length; ++i) {
            EnergyTemp et = ETList.get(i);
            String[] entry = {
                    Integer.toString(bestWorkerId[i]),
                    Integer.toString(et.iter),
                    Double.toString(et.energy),
                    Double.toString(et.temperature)
            };
            entries.add(entry);
        }
    }

    public void addEntry(ArrayList<EnergyTemp> ETList) {
        for (EnergyTemp et : ETList) {
            String[] entry = {
                    Integer.toString(et.iter),
                    Double.toString(et.energy),
                    Double.toString(et.temperature)
            };
            entries.add(entry);
        }
    }

    public void writeCSV() {
        try (CSVWriter writer = new CSVWriter(
                new FileWriter(this.csvName),
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER)) {
            writer.writeAll(entries);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
