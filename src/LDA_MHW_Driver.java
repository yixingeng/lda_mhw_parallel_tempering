import java.io.File;

public class LDA_MHW_Driver {
    //Driver of the manager worker architecture
    public static void main(String[] args) {

        String datasetFilename = "NIPSfromTextForJava.txt";
        String workingDir = System.getProperty("user.dir") + "/";
        String dictName = "NIPSdictionary.txt";
        File dictFile = new File(workingDir + dictName);

        int numWords = 13649;
        int numTopics = 200;//2000;
        int numDocuments = 1740;

        int numIterations = 1500;//100;
        int stepIteration = 10;

        double alpha_k = 0.01;
        double beta_w = 0.001;

        double[] alpha = new double[numTopics];
        for (int i = 0; i < alpha.length; i++) {
            alpha[i] = alpha_k;
        }

        int numWorkers = 20;
        int numSwapping = 150;
        boolean doSwapping = false;

        File docFile = new File(workingDir + datasetFilename);
        File initialTopicAssignment = new File(workingDir + "initial_topic_assignment.txt");

        LDA_MHW_Manager manager = new LDA_MHW_Manager(numTopics, numWords, beta_w,
                alpha, docFile, numDocuments, numIterations, numWorkers,
                stepIteration, numSwapping, dictFile, initialTopicAssignment, doSwapping);
        manager.start();
    }
}
