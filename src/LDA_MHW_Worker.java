import java.io.*;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import cc.mallet.types.Dirichlet;

import java.util.PriorityQueue;
import java.util.*;

public class LDA_MHW_Worker extends Thread {
    private int totalIterations;
    private double temperature;
    private int workerId;
    private int stepIteration;
    private LDA_MHW_Logger logger;
    private LDA_MHW_CSV_Handler csvHandler;
    private ArrayList<EnergyTemp> ETlist = new ArrayList<>();
    public static String[] dict;
    private File topicAssignmentFile;

    protected int[][] wordTopicCounts; //[numWords][numTopics]
    protected int[] topicCounts; //numTopics;
    protected int[][] docTopicCounts; //[numDocuments][numTopics]
    protected int[][] topicAssignments; //[document][word]
    private static int[][] documents;
    private int numDocuments;
    protected final int numTopics;
    protected final int numWords;
    protected double[] alpha;
    protected final double beta;
    protected final double betaSum;
    private double curEnergy;
    private double curPi;

    private boolean continueSwitch = true;
    private int currentIteration = 0;
    protected Random random = new Random();


    protected int[][] docNonzeroTopicIndices; //[numDocuments][numTopics], a list of the indices of topics with non-zero counts per document
    protected int[] docNumNonzero; //[numDocuments], the number of topics with non-zero counts per document.
    int MHWnumCachedSamples;
    int[][] MHWcachedSamples; //[word][sample]
    int[] MHWwordSampleInds; //[word]


    //store these in the class to avoid re-allocating them, but their values will not be read
    //outside of the creation of the samples via the Walker algorithm
    private double[] MHWtempDistribution;
    private double[][] MHWaliasTable;
    private double[][] MHW_L;
    private double[][] MHW_H;

    double[][] MHWstaleDistributions; //[word][K]
    double[] MHWnormalizationConstants;//[word] Corresponds to Q_w in the Metropolis Hastings Walker paper

    public LDA_MHW_Worker(int totalIterations, double temperature, int workerId, int stepIteration,
                          int[][] wordTopicCounts, int[] topicCounts, int[][] docTopicCounts,
                          int[][] documents, int numDocuments, int numTopics, int numWords,
                          double[] alpha, double beta, double betaSum, String[] dict, File topicAssignmentFile) {
        this.totalIterations = totalIterations;
        this.temperature = temperature;
        this.workerId = workerId;
        this.stepIteration = stepIteration;
        this.logger = new LDA_MHW_Logger("worker" + workerId);
        this.csvHandler = new LDA_MHW_CSV_Handler("worker" + workerId + ".csv");
        this.dict = dict;

        this.wordTopicCounts = wordTopicCounts;
        this.topicCounts = topicCounts;
        this.docTopicCounts = docTopicCounts;
        this.topicAssignmentFile = topicAssignmentFile;

        this.documents = documents;
        this.numDocuments = numDocuments;
        this.numTopics = numTopics;
        this.numWords = numWords;
        this.alpha = alpha;
        this.beta = beta;
        this.betaSum = betaSum;


        MHWnumCachedSamples = numTopics;
        MHWtempDistribution = new double[numTopics];
        MHWaliasTable = new double[numTopics][2];
        MHW_L = new double[numTopics][2];
        MHW_H = new double[numTopics][2];
        MHWcachedSamples = new int[numWords][MHWnumCachedSamples];
        initialize(this.documents);
        this.curEnergy = getEnergy();
        ETlist.add(new EnergyTemp(0, this.curEnergy, this.temperature));

    }

    public void pauseWorker() {
        logger.log("Worker " + this.workerId + " pauses");
        this.continueSwitch = false;
    }

    public int checkStatus() {
        return (this.continueSwitch) ? 1 : 0;
    }

    public void stopWorker() {
        System.out.println("Worker " + this.workerId + " stops");
        this.continueSwitch = false;
    }

    public void continueWorker() {
        logger.log("Worker " + this.workerId + " continues");
        this.continueSwitch = true;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        logger.log("set worker " + this.workerId + " temperature: " + temperature);
        this.temperature = temperature;
    }

    private double getEnergy() {
        return -1 * getLogLikelihood();
    }

    private double getLogLikelihood() {
        double logLikelihood = 0.0;

        //calculate phi part
        for (int i = 0; i < numDocuments; i++) {
            double alphaSum = 0.0;
            double logGammaAlphaSum;
            double sumLogGammaAlpha = 0.0;
            double logGammaSumTmp;
            double sumLogGammaTmp = 0.0;
            double sumTmp = 0.0;

            for (int j = 0; j < numTopics; j++) {
                double tmp = docTopicCounts[i][j] + alpha[j];
                alphaSum += alpha[j];
                sumLogGammaAlpha += Dirichlet.logGamma(alpha[j]);
                sumLogGammaTmp += Dirichlet.logGamma(tmp);
                sumTmp += tmp;

            }
            logGammaSumTmp = Dirichlet.logGamma(sumTmp);
            logGammaAlphaSum = Dirichlet.logGamma(alphaSum);
            logLikelihood += logGammaAlphaSum - sumLogGammaAlpha + sumLogGammaTmp - logGammaSumTmp;

        }

        //calculate theta part
        for (int i = 0; i < numTopics; i++) {
            double logGammaBetaSum;
            double sumLogGammaBeta = 0.0;
            double logGammaSumTmp;
            double sumLogGammaTmp = 0.0;
            double sumTmp = 0.0;
            for (int j = 0; j < numWords; j++) {
                double tmp = wordTopicCounts[j][i] + beta;
                sumLogGammaBeta += Dirichlet.logGamma(beta);
                sumLogGammaTmp += Dirichlet.logGamma(tmp);
                sumTmp += tmp;
            }
            logGammaSumTmp = Dirichlet.logGamma(sumTmp);
            logGammaBetaSum = Dirichlet.logGamma(betaSum);
            logLikelihood += logGammaBetaSum - sumLogGammaBeta + sumLogGammaTmp - logGammaSumTmp;
        }
        logger.log("worker " + workerId + " logLikelihood: " + logLikelihood);
        return logLikelihood;
    }

    public double getCurEnergy() {
        return curEnergy;
    }

    public double getCurPi() {
        return curPi;
    }

    public ArrayList<EnergyTemp> getETlist() {
        return this.ETlist;
    }

    protected void updateTopicAssignments(int[][] documents) {
        //for locality of reference, and to avoid reallocation
        int oldTopic;
        int newTopic;
        int word;

        double pi; //Metropolis-Hastings acceptance probability
        double sparseNormalizer;
        double[] sparseDistribution = new double[numTopics];
        boolean chooseSparse;

        int currentTopic; //a temp variable indicating the current true topic index when iterating through non-zero indices

        for (int i = 0; i < documents.length; i++) {
            //sample the topic assignments for each word in the ith document
            for (int n = 0; n < documents[i].length; n++) {
                //remove the current assignment from the sufficient statistics cache

                oldTopic = topicAssignments[i][n];
                word = documents[i][n];

                wordTopicCounts[word][oldTopic]--;
                topicCounts[oldTopic]--;
                docTopicCounts[i][oldTopic]--;

                if (docTopicCounts[i][oldTopic] == 0) {
                    //we deleted a topic for this document, and need to update the doc non-zero topic index cache
                    for (int nonzero_k = 0; nonzero_k < docNumNonzero[i]; nonzero_k++) {
                        currentTopic = docNonzeroTopicIndices[i][nonzero_k];
                        if (currentTopic == oldTopic) {
                            //found it (it must be in there somewhere!)
                            //move the last topic here
                            if (nonzero_k != docNumNonzero[i] - 1 && docNumNonzero[i] > 1) { //if handles edge cases, removing last topic, and removing only topic
                                docNonzeroTopicIndices[i][nonzero_k] = docNonzeroTopicIndices[i][docNumNonzero[i] - 1];
                            }
                            docNumNonzero[i]--;
                            break;
                        }
                    }
                }

                //choose sparse and non-sparse paths from proposal mixture
                sparseNormalizer = 0;
                for (int nonzero_k = 0; nonzero_k < docNumNonzero[i]; nonzero_k++) {
                    currentTopic = docNonzeroTopicIndices[i][nonzero_k];
                    sparseDistribution[nonzero_k] = docTopicCounts[i][currentTopic] * (wordTopicCounts[word][currentTopic] + beta) / (topicCounts[currentTopic] + betaSum);
                    sparseNormalizer += sparseDistribution[nonzero_k];
                }

                chooseSparse = random.nextDouble() < sparseNormalizer / (sparseNormalizer + MHWnormalizationConstants[word]);

                //Draw from proposal
                if (chooseSparse) {
                    newTopic = ProbabilityUtils.sampleFromSparseDiscrete(sparseDistribution, docNonzeroTopicIndices[i], docNumNonzero[i], sparseNormalizer);
                } else {
                    newTopic = getNextSample(word); //Draw from alias table
                }

                //Compute Metropolis-Hastings acceptance ratio
                //model portion of proposal
                if (newTopic >= docTopicCounts[i].length) {
                    System.out.println(newTopic);
                    System.out.println(docTopicCounts[i].length);
                }

                pi = Math.log(docTopicCounts[i][newTopic] + alpha[newTopic]);
                pi -= Math.log(docTopicCounts[i][oldTopic] + alpha[oldTopic]);
                pi += Math.log(wordTopicCounts[word][newTopic] + beta);
                pi -= Math.log(wordTopicCounts[word][oldTopic] + beta);
                pi += Math.log(topicCounts[oldTopic] + betaSum);
                pi -= Math.log(topicCounts[newTopic] + betaSum);

                //The temperature only affects the model portion of the acceptance ratio
                pi /= temperature;

                //contribution from proposal
                //It is easier to just recompute the sparse distribution for this single value, in O(1) time, than to store or find the sparse index and read it from the sparse array.
                pi += Math.log(sparseNormalizer * (docTopicCounts[i][oldTopic] * (wordTopicCounts[word][oldTopic] + beta) / (topicCounts[oldTopic] + betaSum)) + MHWnormalizationConstants[word] * MHWstaleDistributions[word][oldTopic])
                        - Math.log(sparseNormalizer * (docTopicCounts[i][newTopic] * (wordTopicCounts[word][newTopic] + beta) / (topicCounts[newTopic] + betaSum)) + MHWnormalizationConstants[word] * MHWstaleDistributions[word][newTopic]);

                //Accept or reject proposal
                pi = Math.min(1, Math.exp(pi));
                if (random.nextDouble() < pi) {
                    topicAssignments[i][n] = newTopic;
                } else {
                    newTopic = oldTopic;
                }

                //update sufficient statistics cache
                topicCounts[newTopic]++;
                wordTopicCounts[word][newTopic]++;
                docTopicCounts[i][newTopic]++;

                if (docTopicCounts[i][newTopic] == 1) {
                    //we created a new topic for this document, and need to update the doc non-zero topic index cache
                    docNonzeroTopicIndices[i][docNumNonzero[i]] = newTopic; //add the new topic at the right-hand end of the list
                    docNumNonzero[i]++;
                }

                curPi = pi;
            }
        }
    }

    public void run() {
        while (currentIteration < totalIterations) {
            while (continueSwitch) {
                for (int i = 0; i < stepIteration; i++) {
                    updateTopicAssignments(documents);
                    currentIteration++;
                    if (currentIteration == totalIterations) break;
                }
                curEnergy = getEnergy();
                ETlist.add(new EnergyTemp(currentIteration, curEnergy, temperature));
                pauseWorker();
                if (currentIteration == totalIterations) break;
            }

            if (!continueSwitch) {
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            csvHandler.addEntry(ETlist);
            csvHandler.writeCSV();
            saveToText("worker" + workerId + "_");
            checkResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Compute the sufficient statistics from scratch.
     */
    protected void recomputeTextSufficientStatistics(int[][] documents) {

        wordTopicCounts = new int[numWords][numTopics];
        topicCounts = new int[numTopics];
        docTopicCounts = new int[numDocuments][numTopics];

        int word;
        int topic;
        for (int i = 0; i < numDocuments; i++) {
            for (int j = 0; j < documents[i].length; j++) {
                word = documents[i][j];
                topic = topicAssignments[i][j];
                wordTopicCounts[word][topic]++;
                topicCounts[topic]++;
                docTopicCounts[i][topic]++;
            }
        }
    }

    /**
     * Computes a cache of the indices of the topics with non-zero counts for each document
     */
    protected void recomputeDocNonzeroTopicIndices() {
        //assumes docTopicCounts is set.
        int denseTopicInd;
        docNonzeroTopicIndices = new int[numDocuments][numTopics];
        docNumNonzero = new int[numDocuments];
        for (int i = 0; i < numDocuments; i++) {
            denseTopicInd = 0;
            docNumNonzero[i] = 0;
            for (int k = 0; k < numTopics; k++) {
                if (docTopicCounts[i][k] > 0) {
                    docNonzeroTopicIndices[i][denseTopicInd] = k;
                    denseTopicInd++;
                    docNumNonzero[i]++;
                }
            }
        }
    }

    protected int getNextSample(int word) {
        int returner;
        int sampleInd = MHWwordSampleInds[word];
        if (sampleInd < MHWnumCachedSamples) {
            returner = MHWcachedSamples[word][sampleInd];
            MHWwordSampleInds[word]++;
            return returner;
        } else {
            //Draw new samples via Walker's alias method
            //construct new distribution
            double normalizer = 0;
            for (int k = 0; k < numTopics; k++) {
                MHWtempDistribution[k] = alpha[k] * (wordTopicCounts[word][k] + beta) / (topicCounts[k] + betaSum);
                normalizer += MHWtempDistribution[k];
            }
            for (int k = 0; k < numTopics; k++) {
                MHWtempDistribution[k] /= normalizer;

                //save the (soon to be) stale distribution.
                MHWstaleDistributions[word][k] = MHWtempDistribution[k];
            }
            MHWnormalizationConstants[word] = normalizer; //We need this for the proposal

            ProbabilityUtils.generateAlias(MHWtempDistribution, MHWaliasTable, MHW_L, MHW_H); //create alias table
            ProbabilityUtils.sampleAlias(MHWaliasTable, MHWcachedSamples[word]); //get samples

            returner = MHWcachedSamples[word][0]; //the first new sample
            MHWwordSampleInds[word] = 1; //the one after it
            return returner;
        }
    }

    private void loadTopicAssignment(File file) {
        BufferedReader inputStream;
        topicAssignments = new int[numDocuments][];
        try {
            inputStream = new BufferedReader(new FileReader(file));
            String l;
            for (int i = 0; i < numDocuments; i++) {
                this.topicAssignments[i] = new int[documents[i].length];
                l = inputStream.readLine();
                String[] split = l.split(" ");
                //initialize topic assignments uniformly at random
                for (int j = 0; j < topicAssignments[i].length; j++) {
                    this.topicAssignments[i][j] = Integer.parseInt(split[j]);
                }
            }
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.log("Complete loading topic assignment");

    }

    protected void initialize(int[][] documents) {
        loadTopicAssignment(topicAssignmentFile);

        //initialize count matrices
        recomputeTextSufficientStatistics(documents);
        //initialize cache of non-zero topics per document
        recomputeDocNonzeroTopicIndices();

        MHWstaleDistributions = new double[numWords][numTopics];
        MHWnormalizationConstants = new double[numWords];

        MHWwordSampleInds = new int[numWords];
        for (int i = 0; i < numWords; i++) {
            MHWwordSampleInds[i] = MHWnumCachedSamples;
            getNextSample(i); //creates samples for this word and sets the mixture weight for it
            MHWwordSampleInds[i] = 0;
        }
        logger.log("Complete initialization.");
    }

    public int[][] getWordTopicCounts() {
        return wordTopicCounts;
    }

    public int[][] getDocTopicCounts() {
        return docTopicCounts;
    }

    public int getWorkerId() {
        return this.workerId;
    }

    public void saveToText(String baseFilename) throws IOException {
        final String topicAssignmentName = "topicAssignments.txt";
        final String wordTopicCountsName = "wordTopicCounts.txt";
        final String docTopicCountsName = "docTopicCounts.txt";

        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(baseFilename + topicAssignmentName);
            for (int i = 0; i < topicAssignments.length; i++) {
                //sample the topic assignments for each word in the ith document
                for (int n = 0; n < topicAssignments[i].length; n++) {
                    outputStream.print(topicAssignments[i][n]);
                    if (n < topicAssignments[i].length) {
                        outputStream.print(" ");
                    }
                }
                outputStream.println("");
            }
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }

        int[][] wordTopicCounts = getWordTopicCounts();

        outputStream = null;
        try {
            outputStream = new PrintWriter(baseFilename + wordTopicCountsName);
            for (int i = 0; i < wordTopicCounts.length; i++) {
                for (int n = 0; n < wordTopicCounts[i].length; n++) {
                    outputStream.print(wordTopicCounts[i][n]);
                    if (n < wordTopicCounts[i].length) {
                        outputStream.print(" ");
                    }
                }
                outputStream.println("");
            }
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }

        int[][] docTopicCounts = getDocTopicCounts();

        outputStream = null;
        try {
            outputStream = new PrintWriter(baseFilename + docTopicCountsName);
            for (int i = 0; i < docTopicCounts.length; i++) {
                for (int n = 0; n < docTopicCounts[i].length; n++) {
                    outputStream.print(docTopicCounts[i][n]);
                    if (n < docTopicCounts[i].length) {
                        outputStream.print(" ");
                    }
                }
                outputStream.println("");
            }
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    class Results implements Comparable<Results> {
        public double value;
        public int id;

        public Results(double value, int id) {
            this.value = value;
            this.id = id;
        }

        @Override
        public int compareTo(Results o) {
            if (o.value > this.value) {
                return 1;
            } else {
                if (o.value < this.value) return -1;
            }
            return 0;
        }
    }

    private void checkResult() {
        double[][] wordTopicResults = new double[numWords][numTopics];
        double[] sumResults = new double[numTopics];

        ArrayList<PriorityQueue> al = new ArrayList<>();
        for (int i = 0; i < numWords; ++i) {
            for (int j = 0; j < numTopics; ++j) {
                wordTopicResults[i][j] = wordTopicCounts[i][j] + alpha[j];
                sumResults[j] += wordTopicResults[i][j];
            }
        }
        for (int j = 0; j < numTopics; ++j) {
            PriorityQueue<Results> queue = new PriorityQueue<>();
            for (int i = 0; i < numWords; ++i) {
                Results r = new Results(wordTopicResults[i][j] / sumResults[j], i);
                queue.add(r);
            }
            al.add(queue);
        }
        try {
            exportResults(al);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exportResults(ArrayList<PriorityQueue> al) throws IOException {
        PrintWriter wordOutputStream = null;
        PrintWriter valueOutputStream = null;
        String wordResultFileName = "worker" + workerId + "_wordResult.txt";
        String valueResultFileName = "worker" + workerId + "_valueResult.txt";
        int topWordsNum = 20;
        try {
            wordOutputStream = new PrintWriter("./" + wordResultFileName);
            valueOutputStream = new PrintWriter("./" + valueResultFileName);

            for (int i = 0; i < numTopics; ++i) {
                PriorityQueue<Results> pq = al.get(i);
                wordOutputStream.format("%d ", i);
                valueOutputStream.format("%d ", i);
                for (int j = 0; j < topWordsNum; ++j) {
                    Results r = pq.remove();
                    wordOutputStream.format("%s ", dict[r.id]);
                    valueOutputStream.format("%f ", r.value);
                }
                wordOutputStream.format("\n");
                valueOutputStream.format("\n");
            }
        } finally {
            wordOutputStream.close();
            valueOutputStream.close();
        }
    }
}